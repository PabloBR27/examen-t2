﻿using ProyectoT2.DB.Mapping;
using ProyectoT2.Models;
using Microsoft.EntityFrameworkCore;

namespace ProyectoT2.DB
{
    public class UsuarioContext : DbContext
    {
        public DbSet<Usuario> Usuarios { get; set; }
        public DbSet<Pokemon> Pokemones { get; set; }
        public DbSet<UsuarioPokemon> PokemonesUsuario { get; set; }

        public UsuarioContext(DbContextOptions<UsuarioContext> options) : base(options) { }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new UsuarioMap());
            modelBuilder.ApplyConfiguration(new PokemonMap());
            modelBuilder.ApplyConfiguration(new UsuarioPokemonMap());
        }

    }
}
