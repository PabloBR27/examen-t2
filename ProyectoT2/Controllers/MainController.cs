﻿using System.IO;
using ProyectoT2.DB;
using ProyectoT2.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using ProyectoT2.Controllers;
using System.Collections.Generic;
using System;

namespace ProyectoT2.Controllers
{
    public class MainController : Controller
    {
        private IWebHostEnvironment hosting;
        private UsuarioContext context;
        public MainController(UsuarioContext context, IWebHostEnvironment hosting)
        {
            this.context = context;
            this.hosting = hosting;
        }

        public IActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public IActionResult MuestraPokemon()
        {
            var Pokemon = ListaPokemon();

            return View("MuestraPokemon", Pokemon);
        }
        [HttpGet]
        public IActionResult RegistraPokemon()
        {
            return View();
        }
        [HttpPost]
        public IActionResult RegistraPokemon(Pokemon pokemon, IFormFile imagen)
        {

            var pok = context.Pokemones;
            List<Pokemon> list = pok.ToList();
            if (list.Count() !=0)
            {
                foreach (Pokemon po in list)
                {
                    try
                    {
                        if (!po.Nombre.Equals(pokemon.Nombre))
                        {
                            pokemon.Imagen = SaveFile(imagen);
                            context.Pokemones.Add(pokemon);
                            context.SaveChanges();
                        }
                        else
                        {
                            TempData["AuthMessage"] = "El nombre ya existe, intenta poner otro";
                            return RedirectToAction("RegistraPokemon");
                        }
                    }
                    catch(Exception e)
                    {

                    }
                   
                }
            }
            else
            {
                pokemon.Imagen = SaveFile(imagen);
                context.Pokemones.Add(pokemon);
                context.SaveChanges();

            }
            

            return  RedirectToAction("MuestraPokemon");
        }
        private string SaveFile(IFormFile file)
        {
            string relativePath = "";

            if (file.Length > 0 && file.ContentType == "image/png")
            {
                relativePath = Path.Combine("files", file.FileName);
                var filePath = Path.Combine(hosting.WebRootPath, relativePath);
                var stream = new FileStream(filePath, FileMode.Create);
                file.CopyTo(stream);
                stream.Close();
            }

            return "/" + relativePath.Replace('\\', '/');
        }
        public IActionResult MuestraPokemonesUsuario()
        {

            var Pokemon = context.Pokemones;
            List<UsuarioPokemon> UserPokes = context.PokemonesUsuario.Where(u => u.Iduser == GetLoggedUser().Id).ToList();
            List<Pokemon> PokesUsuario = new List<Pokemon>();

            foreach(UsuarioPokemon up in UserPokes)
            {
                if(up.Iduser == GetLoggedUser().Id)
                {
                    Pokemon poke = context.Pokemones.First(p => p.Id == up.IdPokemon);
                    PokesUsuario.Add(poke);
                }
            }

            //return View("MuestraPokemonesUsuario", PokesUsuario);
            return View("MuestraPokemonesUsuario", PokesUsuario);
        }
        [HttpPost]
        public IActionResult CapturaPokemon(int id)
        {
            UsuarioPokemon userP = new UsuarioPokemon
            { Iduser = GetLoggedUser().Id,
                IdPokemon = id 
            };
            context.PokemonesUsuario.Add(userP);
            context.SaveChanges();

            return RedirectToAction("MuestraPokemon");
        }
        public IActionResult LiberaPokemon(int id)
        {
            var pkUser = context.PokemonesUsuario.First(pu=>pu.IdPokemon == id);
            context.Remove(pkUser);
            context.SaveChanges();

            return RedirectToAction("MuestraPokemonesUsuario");
        }

        public IActionResult BuscaPokemon(string n)
        {
            var Pok = context.Pokemones.FirstOrDefault(p=>p.Nombre==n);
            Pokemon pokemon = Pok;
            return View("BuscaPokemon", pokemon);
        }

        private Usuario GetLoggedUser()
        {
            var claim = HttpContext.User.Claims.First();
            string username = claim.Value;
            Usuario user = context.Usuarios.First(o => o.Username == username);
            return user;
        }
        private List<Pokemon> ListaPokemon()
        {
            var Poke = context.Pokemones;
            List<Pokemon> list = Poke.ToList();
            return list;
        }
    }
}
