﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoT2.Models
{
    public class UsuarioPokemon
    {
        public int Id { get; set; }
        public int IdPokemon { get; set; }
        public int Iduser { get; set; }
    }
}
